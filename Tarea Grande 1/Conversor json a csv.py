import random
import json 
import csv


def fecha_random():
    m=random.randint(1,12)
    a=random.randint(2000,2016)
    if a==2016 or 2012 or 2008 or 2004 or 2000 and m==2:
        d=random.randint(1,29)
    elif m==2:
        d=random.randint(1,28)
    else:
        d=random.randint(1,30)
    fecha=str(d)+"/"+str(m)+"/"+str(a)

    return(fecha)
    
f = open('tarea1.json') 
data = json.load (f) 

titulos=["Expedicion","Andinista","Pais","Cumbre","Latitud","Longitud","Fecha"]
doc=[]
doc.append(titulos)

x=1
for i in data["expediciones"]:
    for n in i["cumbres"]:
        fecha=fecha_random()
        for p in i["andinistas"]:
            lista=[]
            lista.append(x)
            lista.append(p["numero"])
            lista.append(p["pais"])
            lista.append(n["nombre"])
            lista.append(n["lat"])
            lista.append(n["lon"])
            lista.append(fecha)
            doc.append(lista)
            
    x+=1
    
newline=""
with open("data.csv", "w", newline="") as file:
    writer = csv.writer(file)
    for row in doc:
        writer.writerow(row)
file.close()
