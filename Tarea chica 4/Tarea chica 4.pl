padre(juan, amanda).
madre(ximena, amanda).

padre(juan, patricio).
madre(ximena, patricio).

padre(juan, ana).
madre(ximena, ana).

padre(andres, juan).
madre(laura, juan).

padre(andres, luis).
madre(laura, luis).

padre(luis,jorge).
madre(cami,jorge).

padre(patricio, bonifacio).
madre(paula, bonifacio).

padre(jorge, martin).
madre(pilar, martin).


progenitor(X,Y) :- padre(X,Y).
progenitor(X,Y) :- madre(X,Y).



ancestro(X,Y,G) :- progenitor(X,Y),
             	   G is 1.

ancestro(X,Y,Grado) :- progenitor(X,Z),
             	   ancestro(Z,Y,G),
             	   Grado is G + 1.


hermano(X,Y) :- padre(Z,X),
    			padre(Z,Y),
    			X \= Y.

primo(X,Y) :- padre(Z,X),
    		  padre(U,Y),
    		  hermano(Z,U).

tio(X,Y) :- padre(Z,Y),
    		hermano(X,Z).

%Primos en grado K
primos(Grado,X,Y) :- ancestro(Z,X,Grado),
    				 ancestro(U,Y,Grado), 
                  	 hermano(Z,U).  


suma(0,X,X).
suma(s(X),s(Y),Z):- suma(X,Y,Z).